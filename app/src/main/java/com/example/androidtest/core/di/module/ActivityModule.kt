package com.example.androidtest.core.di.module

import com.example.androidtest.core.ui.CoreActivity
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun bindCoreActivity(): CoreActivity

}
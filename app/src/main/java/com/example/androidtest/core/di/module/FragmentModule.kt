package com.example.androidtest.core.di.module

import com.example.androidtest.login.ui.LoginFragment
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun bindLoginFragment(): LoginFragment

}
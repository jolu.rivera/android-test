package com.example.androidtest.core.di.module

import androidx.lifecycle.ViewModel
import com.example.androidtest.core.di.annotation.ViewModelKey
import com.example.androidtest.login.viewmodel.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

}
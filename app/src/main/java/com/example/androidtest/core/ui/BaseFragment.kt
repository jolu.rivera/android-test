package com.example.androidtest.core.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.androidtest.BR
import com.example.androidtest.core.viewmodel.BaseViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

/**
 *
 * @param VIEW_MODEL refers to the ViewModel class of the feature.
 *
 * @param DATA_BINDING refers to the auto-generated DataBinding class for your fragment.
 *
 *
 */
abstract class BaseFragment<VIEW_MODEL : BaseViewModel<*>,
        DATA_BINDING : ViewDataBinding> :
    Fragment() {

    protected lateinit var mViewModel: VIEW_MODEL
    private lateinit var mViewDataBinding: DATA_BINDING
    private lateinit var mBackCallback: OnBackPressedCallback

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract fun getViewModelClass(): Class<VIEW_MODEL>

    /**
     * @return the layout resource id of your fragment's view.
     */
    @LayoutRes
    abstract fun getLayoutRes(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProvider(this, viewModelFactory)[getViewModelClass()]
        if (handleBackCallback()) initializeBackPressedCallback()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false)
        mViewDataBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            setVariable(BR.viewModel, mViewModel)
            getFragmentView()?.let {
                setVariable(BR.viewInteraction, it)
            }
            this@BaseFragment.setupBeforeBind(this)
            executePendingBindings()
        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
    }

    /**
     * Override this method to perform operations before executing all
     * pending bindings.
     */
    protected open fun setupBeforeBind(mViewDataBinding: DATA_BINDING) {
        // Override to do whatever you need here
    }

    /**
     * Override this method to allow your fragment to handle back
     * event at [onBackPressed].
     */
    protected open fun handleBackCallback(): Boolean = false

    /**
     * This method allow us to call the view interface from XML file
     * by using data binding. In order to make it work, you must implement
     * some interface that extends from [BaseFragmentView].
     */
    private fun getFragmentView(): BaseFragmentView? =
        if (this is BaseFragmentView) this else null

    /**
     * Override this method to handle back event only if you
     * got true at [handleBackCallback].
     */
    protected open fun onBackPressed() {
        // Override to handle back event
    }

    /**
     * Override this method to implement the handling for ViewStates
     */
    protected open fun renderViewState(state: BaseViewState) {
        // Override this method if is required
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mViewModel.clearState()
    }

    private fun initializeBackPressedCallback() {
        mBackCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, mBackCallback)
    }

    private fun setUpObservers() {
        mViewModel.let { viewModel ->
            viewModel.uiState.onEach { state -> state?.let { renderViewState(it) } }
        }
    }
}

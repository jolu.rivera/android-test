package com.example.androidtest.core.ui

abstract class BaseViewState {
    object None : BaseViewState()
}
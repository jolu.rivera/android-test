package com.example.androidtest.core.ui

import android.os.Bundle
import com.example.androidtest.R
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity

class CoreActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.core_activity)
    }
}

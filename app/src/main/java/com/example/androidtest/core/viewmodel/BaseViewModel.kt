package com.example.androidtest.core.viewmodel

import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.ViewModel
import com.example.androidtest.core.ui.BaseViewState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import com.example.androidtest.core.data.BaseRepository

open class BaseViewModel<REPOSITORY : BaseRepository>(protected val repository: REPOSITORY? = null) :
    ViewModel(),
    Observable {

    private val _uiState = MutableStateFlow<BaseViewState>(BaseViewState.None)
    val uiState: StateFlow<BaseViewState?> = _uiState

    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry() }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.add(callback)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        callbacks.remove(callback)
    }

    fun notifyChange() {
        callbacks.notifyCallbacks(this, 0, null)
    }

    fun notifyPropertyChanged(fieldId: Int) {
        callbacks.notifyCallbacks(this, fieldId, null)
    }

    fun clearState() {
        updateState(BaseViewState.None)
    }

    protected fun updateState(state: BaseViewState) {
        _uiState.tryEmit(state)
    }


}
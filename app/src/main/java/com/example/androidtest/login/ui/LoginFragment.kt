package com.example.androidtest.login.ui

import android.widget.Toast
import com.example.androidtest.R
import com.example.androidtest.core.ui.BaseFragment
import com.example.androidtest.databinding.FragmentLoginBinding
import com.example.androidtest.login.viewmodel.LoginViewModel

class LoginFragment : BaseFragment<LoginViewModel, FragmentLoginBinding>(), LoginFragmentView {

    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun getLayoutRes(): Int = R.layout.fragment_login

    override fun getStarted() {
        // call viewmodel or navigation here
        Toast.makeText(
            requireContext(),
            "Clicked! ${mViewModel.inputGetStartedValue}",
            Toast.LENGTH_LONG
        )
            .show()
    }

    override fun login() {
        // call viewmodel or navigation here
        Toast.makeText(
            requireContext(),
            "Clicked! ${mViewModel.inputLoginValue}",
            Toast.LENGTH_LONG
        )
            .show()
    }
}
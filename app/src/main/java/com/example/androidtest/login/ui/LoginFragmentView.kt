package com.example.androidtest.login.ui

import com.example.androidtest.core.ui.BaseFragmentView

interface LoginFragmentView : BaseFragmentView {

    fun getStarted()
    fun login()

}
package com.example.androidtest.login.viewmodel

import androidx.databinding.Bindable
import com.example.androidtest.BR
import com.example.androidtest.core.data.BaseRepository
import com.example.androidtest.core.viewmodel.BaseViewModel
import javax.inject.Inject

class LoginViewModel @Inject constructor() : BaseViewModel<BaseRepository>() {

    var inputGetStartedValue: String = ""
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.inputGetStartedValue)
        }

    var inputLoginValue: String = ""
        @Bindable get
        set(value) {
            field = value
            notifyPropertyChanged(BR.inputLoginValue)
        }

}